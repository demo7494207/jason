import { test, expect } from "@playwright/test";
import LandingPage from "../pages/LandingPage";
test.beforeEach(async ({ page }) => {
  const landingPage = new LandingPage(page);
  await page.goto("/");
  await landingPage.login();
});
test("Verify that the items are sorted by Name ( A-Z ).", async ({ page }) => {
  const landingPage = new LandingPage(page);
  await landingPage.verifyUserLoggedIn();
  await landingPage.checkSort();
});
test("Verify that the items are sorted by Name ( Z-A ).", async ({ page }) => {
  const landingPage = new LandingPage(page);
  await landingPage.verifyUserLoggedIn();
  await landingPage.checkReverseSort();
});
