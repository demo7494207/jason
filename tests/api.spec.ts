import { test, expect } from "@playwright/test";

test("Verify count of Authentication & Authorization category", async ({
  request,
}) => {
  const url = "https://api.publicapis.org/entries";
  const param = "Authentication & Authorization";
  // Extract the API response

  const response = await request.get(url);

  //ensure the response is successful
  expect(response.ok()).toBeTruthy();
  const apiResponse = await response.json();

  // Find all objects with property "Category: Authentication & Authorization"
  const authAndAuthObjects = apiResponse.entries.filter((entry: any) =>
    entry.Category.includes(param),
  );

  // Count the number of objects
  const count = authAndAuthObjects.length;

  // Print the count
  console.log(
    `Number of objects with Category "Authentication & Authorization": ${count}`,
  );

  // Print found objects
  console.log("Found Objects:");
  console.log(authAndAuthObjects);
});
