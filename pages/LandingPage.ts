import { expect, Locator, Page } from "@playwright/test";
export default class LoginPage {
  readonly page: Page;
  readonly usename_text_box: Locator;
  readonly password_text_box: Locator;
  readonly login_button: Locator;
  readonly menu_button: Locator;
  readonly inventory_items: Locator;
  readonly sortorder_za: Locator;
  readonly sort_items: Locator;

  constructor(page: Page) {
    this.page = page;
    this.usename_text_box = page.locator('input[id="user-name"]');
    this.password_text_box = page.locator('input[id="password"]');
    this.login_button = page.locator('[id="login-button"]');
    this.menu_button = page.locator('[id="react-burger-menu-btn"]');
    this.inventory_items = page.locator(".inventory_item_name");
    this.sortorder_za = page.locator('[data-test="product_sort_container"]');
    this.sort_items = page.locator('[data-test="product_sort_container"]');
  }

  async enterName(name: string) {
    await this.usename_text_box.fill(name);
  }

  async enterPassword(pwd: string) {
    await this.password_text_box.fill(pwd);
  }
  async login(
    username: string = "standard_user",
    password: string = "secret_sauce",
  ) {
    await this.usename_text_box.clear();
    await this.usename_text_box.first().fill(username);
    await this.password_text_box.first().clear();
    await this.password_text_box.first().fill(password);
    await this.login_button.click();
  }

  arraysAreEqual(array1: string[], array2: string[]) {
    // Check if arrays have the same length
    if (array1.length !== array2.length) {
      return false;
    }

    // Check each element in the arrays
    for (let i = 0; i < array1.length; i++) {
      if (array1[i] !== array2[i]) {
        return false;
      }
    }
    // If all elements match and lengths are the same, arrays are equal
    return true;
  }

  async verifyUserLoggedIn() {
    await expect(this.menu_button).toBeVisible();
  }
  async checkSort() {
    await this.sort_items.click();
    await this.sortorder_za.waitFor();
    await this.sortorder_za.selectOption("az");
    await this.page.keyboard.press("Escape");
    const items = await this.inventory_items.allInnerTexts();
    const sortedItems = items.slice().sort();
    expect(this.arraysAreEqual(items, sortedItems)).toBeTruthy();
  }

  async checkReverseSort() {
    await this.sort_items.click();
    await this.sortorder_za.waitFor();
    await this.sortorder_za.selectOption("za");
    await this.page.keyboard.press("Escape");
    const items = await this.inventory_items.allInnerTexts();
    const sortedItems = items.slice().sort().reverse();
    expect(this.arraysAreEqual(items, sortedItems)).toBeTruthy();
  }
  async visitPage() {
    await this.page.goto("https://www.saucedemo.com/");
  }
}
