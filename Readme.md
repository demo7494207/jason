# Running Automated Tests with Playwright and TypeScript

This repository contains automated tests for JASON using Playwright with TypeScript.

## Prerequisites

Before running the tests, ensure you have the following prerequisites installed:

- Node.js (v14.x or higher)
- npm (v6.x or higher)

## Installation

1. Clone this repository to your local machine:

```bash
git clone https://gitlab.com/demo7494207/jason.git
```

2. Navigate to the project directory:

```bash
cd your-repository
```

3. Install dependencies:

```bash
npm install
```

## Running Tests Locally

To run the tests locally on your machine, follow these steps:

1. Execute the following command:

```bash
npx playwright test run test
```

This command will compile TypeScript files and execute Playwright tests.

## Running Tests on CI

This project is configured to run automated tests on Continuous Integration (CI) Gitlab  and the config file can be found on gitlab-ci.yml
`


## Configuration

You can configure the test environment, browser, and other settings in the `playwright.config.ts` file located in the `src` directory.

## Writing Tests

Tests are written in TypeScript using the Playwright framework. You can find test files in the `/tests` directory.

## Troubleshooting

If you encounter any issues while setting up or running tests, please refer to the [Playwright documentation](https://playwright.dev/docs/intro) or open an issue in this repository.

